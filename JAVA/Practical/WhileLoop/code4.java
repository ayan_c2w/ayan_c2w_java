
class Demo {
	public static void main(String[]args) {
		char ch = 'A';
		int i = 1;
		while(i<=6){
			if (i%2==1) {
				System.out.print(ch+" ");
			} else{
				System.out.print(i+" ");
			}
			ch++;
			i++;
		}
	}
}
