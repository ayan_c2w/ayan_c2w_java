
import java.io.*;
class Demo {
	public static void main (String[]args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int rows = Integer.parseInt(br.readLine());
		char ch = 'J';
			for (int i=1;i<=rows;i++){
				for (int j=1;j<=rows-i+1;j++){
					System.out.print(ch-- +" ");
				}
				System.out.println();
			}
	}
}
