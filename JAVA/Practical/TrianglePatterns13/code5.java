
import java.io.*;
class Demo {
	public static void main (String[]args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int rows = Integer.parseInt(br.readLine());
		
			for (int i=1;i<=rows;i++){
				char ch1 ='A';
				char ch2 ='a';
				for (int j=1;j<=rows-i+1;j++){
					if(i%2==1) {
						System.out.print(ch1++ +" ");
					} else{
						System.out.print(ch2++ +" ");
					}

				}
				System.out.println();
			}
			
	}
}


