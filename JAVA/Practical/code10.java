
import java.io.*;
class Demo {
	public static void main (String[]args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int rows = Integer.parseInt(br.readLine());
		char ch = 'a';
		int num =1;
			for (int i=1;i<=rows;i++){
				
				for (int j=1;j<=i;j++){
					if (i%2==1) {
						System.out.print(num++ +" ");
					} else{
						System.out.print(ch+" ");
					}
					ch++;
				
				}
				System.out.println();

			}
	
	}
}
