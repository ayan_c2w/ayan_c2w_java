
import java.io.*;
class Demo {
	public static void main (String[]args) throws IOException{
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int rows = Integer.parseInt(br.readLine());
		    for (int i=1;i<=rows;i++) {
			    char ch = 'd';
			    char ph = 'D';
			    for(int j=1;j<=i;j++) {
				    if (i%2==1) {
					    System.out.print(ph-- +" ");
				    } else{
					    System.out.print(ch-- +" ");
				    }
				   
			    }
				    System.out.println();
		    }
	
	}
}
