
import java.io.*;
class Demo {
	public static void main (String[]args)throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int rows = Integer.parseInt(br.readLine());
		 char ch = 'a';
			for (int i=1;i<=rows;i++){
				int num =i;
				for (int j=1;j<=i;j++){
					if(j%2==0){
						System.out.print(ch++ +" ");
					} else {
						System.out.print(num +" ");
					}
				}
				System.out.println();
			}
	}
}

					
