

class ShortDemo {
	public static void main (String[]args){
		short x = 'A';
		short y = '1';
		System.out.println(x);    // ASCII value of A which is 65
		System.out.println(y);	// ASCII value os 1 
	}
}
