


class FloatDemo {
	public static void main (String[]args){
		float num1 = 90.89f;
		System.out.println(num1);    // 90.89
		double num2 = num1;
		System.out.println(num1);    //cant convert into double to float
	}
}

