// Q.3 by if else 


class Demo { 
	public static void main (String[]args) {
		String ch = "xl";
		if (ch == "s") {
			System.out.println("Small");
		} else if (ch == "m" ) {
			System.out.println("Medium");
		} else if (ch == "l") {
			System.out.println("Large");
		} else {
			System.out.println("Extra Large");
		}
	}
}

