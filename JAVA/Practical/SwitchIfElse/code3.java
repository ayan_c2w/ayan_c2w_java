// Q .2     by if else

class Demo { 
	public static void main (String[]args) {
		char ch = 'l';
		if (ch== 'O') {
			System.out.println(ch+" Outstanding Grade");
		} else if (ch=='A') {
			System.out.println(ch+" Excellent Grade");
		} else if (ch == 'B') {
			System.out.println(ch+" Good Grade");
		} else {
			System.out.println(ch+" Poor Grade");
		}
	}
}


