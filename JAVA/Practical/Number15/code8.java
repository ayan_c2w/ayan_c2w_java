import java.io.*;
class Demo {
	public static void main (String[]args) throws IOException{
		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));
		int num = Integer.parseInt(br.readLine());
		int var = 0;
		while(num!=0){
			int rem = num%10;
			var = var*10+rem;
			num/=10;
		}
		System.out.println(var);
	}
}

