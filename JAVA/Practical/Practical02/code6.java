

class IfDemo {
	public static void main  (String[]args) {
		int x = 20;
		if (x%7==0) {
			System.out.println(x+" is Divisible by 7");
		}  else if (x%3==0) {
			System.out.println(x+" is Divisible by 3");
		} else{
			System.out.println(x+" is neither Divisible by 3 nor 7");
		}
	}
}

