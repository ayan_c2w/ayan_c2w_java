class IfDemo {
	public static void main(String[]args){
		float x = 900.0f;     // selling price
		float y = 900.0f;	// cost price
	        float z = (x-y);     // s.p - cp
		if (z>0) {
			System.out.println(z+" rs of  profit ");
		} else if (z<0) {
			System.out.println((-z)+" rs of  loss ");
		}else {
			System.out.println(" No profit no loss ");
		}
	}
}

