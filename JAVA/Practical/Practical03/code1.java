

class IfDemo {
	public static void main (String[]args){
		int num = -11;
		if ((1<=num) && (num <=1000)) {
			System.out.println(num +" is in the range 1 to 1000");
		} else {
			System.out.println(num +" is not in the range 1 to 1000");
		}
	}
}

