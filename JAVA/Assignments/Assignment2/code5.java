

class BitwiseOPerator {
	public static void main (String[]args){
		int x = 20;
		int y = 15;
		System.out.println("bitwise AND operator = "+(x&y));
		System.out.println("bitwise OR operator = "+(x|y));
		System.out.println("bitwise XOR operator = "+(x^y));
		System.out.println("bitwise left shift of x = "+(x<<2));
		System.out.println("bitwise left shift of y = "+(y<<2));
		System.out.println("bitwise Right shift of x = "+(x>>1));
		System.out.println("bitwise Right shift of y = "+(y>>1));
	}
}



