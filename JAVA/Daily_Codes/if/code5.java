

class Switch {
	public static void main (String[]args) {
		int x = 1;
		System.out.println("before Switch");
		switch (x) {
		
		case 1:
			System.out.println("one");
			break;
		case 2:
			System.out.println("two");
			break;
		case 3:
			System.out.println("three");
			break;
		default:
			System.out.println("In default state");
		
		}
		System.out.println("end Switch");
	}
}

